<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
  **************************************************************************
  *                                                                        *
  *  Version 2.0 of the first detailed Spd description by Grigori Rybkine  *
  *                         Grigori.Rybkine@cern.ch                        *
  *                                                                        *
  **************************************************************************
-->

<!-- ## Scintillator Pad Detector (Spd) Geometry Parameters Definition ## -->

<!-- %%%%%%                   Spd Basic Parameters                 %%%%%% -->
  <parameter name = "SpdTiltAngle"          value = "0.207*degree"/>

  <parameter name = "SpdDY1"     value ="0.0*mm" />
  <parameter name = "SpdDY2"     value ="0.0*mm" />
  <parameter name = "SpdDY3"     value ="0.0*mm" />
  <parameter name = "SpdDY4"     value ="0.0*mm" />
  <parameter name = "SpdDY5"     value ="0.0*mm" />
  <parameter name = "SpdDY6"     value ="0.0*mm" />
  <parameter name = "SpdDY7"     value ="0.0*mm" />
  <parameter name = "SpdDY8"     value ="0.0*mm" />

  <parameter name = "SpdXMargin"     value ="8000.0*mm" />
  <parameter name = "SpdYMargin"     value ="50.0*mm" />
  <parameter name = "SpdZMargin"     value ="5.0*mm" />

  <parameter name = "SpdHoleMargin"     value ="50.0*mm" />



<!-- ###             The Basic Sizes: SpdModule, SpdFrame             ### -->
  <parameter name = "SpdModXYSize"          value = "476.*mm"/>
  <parameter name = "SpdModLength"          value = "33.5*mm"/>
  <parameter name = "SpdFrameLength"          value = "40.*mm"/>

<!-- ### For boolean operations "SpdFake" is used: must be greater than 1 -->
  <parameter name = "SpdFake"             value = "2"/>

<!-- ###    Inner Section:  4 * 3 Module Sizes -  1 * 1 Module Sizes  ### -->
  <parameter name = "SpdInnXSize"        value = "4*SpdModXYSize"/>
  <parameter name = "SpdInnYSize"        value = "3*SpdModXYSize"/>

<!-- ##    Middle Section:  8 * 5 Module Sizes - 4 * 3 Modules Sizes   ## -->
  <parameter name = "SpdMidXSize"        value = "8*SpdModXYSize"/>
  <parameter name = "SpdMidYSize"        value = "5*SpdModXYSize"/>

<!-- ##   Outer Section:  16 * 13 Module Sizes - 8 * 5 Module Sizes    ## -->
  <parameter name = "SpdOutXSize"        value = "16*SpdModXYSize"/>
  <parameter name = "SpdOutYSize"        value = "13*SpdModXYSize"/>

<!-- ##     Dimensions of Spd     ## -->
  <parameter name = "SpdXSize" value = "SpdOutXSize"/>
  <parameter name = "SpdYSize" value = "SpdOutYSize"/>
  <parameter name = "ToleranceZ1" value ="0.04*mm"/>
  <parameter name = "ToleranceZ2" value ="0.02*mm"/>
  <parameter name = "ToleranceZ3" value ="0.04*mm"/>
  <parameter name = "SpdZSize"
   value = "SpdFrameLength+SpdModLength+ToleranceZ1+ToleranceZ2+ToleranceZ3"/>

<!-- Spd Frame and Modules Plane Geometry Center Offset to the Center of Spd-->
  <parameter name = "SpdFrameOffset"
            value = "-SpdZSize/2.+SpdFrameLength/2.+ToleranceZ1"/>
  <parameter name = "SpdPlaneOffset"
            value = "SpdZSize/2.-SpdModLength/2.-ToleranceZ3"/>

<!--                 Spd Modules Basic Parameters                -->
<!--  Spd Modules consist of Box with Box Cover, layer of Fiber Loops,
      layer filled with scintillator Tiles (or Cells), and Box Bottom   -->
  <parameter name = "SpdBoxCoverThick"       value = "2.0*mm"/>
  <parameter name = "SpdBoxBottomThick"       value = "2.0*mm"/>
  <parameter name = "SpdBoxBarBot"         value = "8.0*mm"/>
  <parameter name = "SpdBoxBarTop"        value = "2.0*mm"/>
  <parameter name = "SpdBoxSideThick"        value = "0.35*mm"/>
  <parameter name = "SpdBoxSideBend"        value = "6.0*mm"/>
  <parameter name = "SpdBoxXYSize"        value = "SpdModXYSize"/>
  <parameter name = "SpdBoxHeight"        value = "SpdModLength"/>
  <parameter name = "SpdCellThick"        value = "15.0*mm"/>
  <parameter name = "SpdPaperThick"       value = "0.15*mm"/>
  <parameter name = "SpdScinThick"        value = "15.5*mm"/>

  <parameter name = "SpdTolerance" value ="0.03*mm"/>
  <parameter name = "LayTolerance" value ="0.05*mm"/>
  <parameter name = "SpdModFibersThick"
            value = "SpdBoxHeight-SpdBoxCoverThick-SpdScinThick-SpdBoxBottomThick-
                    2.*SpdTolerance-3.*LayTolerance"/>

<!-- ### Z Geometry ### -->
<!-- ## Geometry Centers of Box Cover, layer of Fiber Loops,
      layer filled with scintillator Tiles (or Cells), and Box Bottom
      are offset from the Geometry Center of Spd Module (Box)         ## -->
  <parameter name = "SpdBoxCoverOffset"
            value = "SpdBoxCoverThick/2.-SpdBoxHeight/2.+SpdTolerance"/>
  <parameter name = "SpdBoxBottomOffset"
            value = "SpdBoxHeight/2.-SpdBoxBottomThick/2.-SpdTolerance"/>
  <parameter name = "SpdModFibersOffset" value =
    "SpdBoxCoverOffset+SpdBoxCoverThick/2.+SpdModFibersThick/2.+LayTolerance"/>
  <parameter name = "SpdScinOffset" value =
    "SpdModFibersOffset+SpdModFibersThick/2.+SpdScinThick/2.+LayTolerance"/>

<!-- ### XY Geometry ### -->
  <parameter name = "SpdScinXYSize" value = "SpdModXYSize-2*SpdBoxSideThick"/>
  <parameter name = "SpdScinXYSizeHalf"
             value = "SpdModXYSize/2-SpdBoxSideThick"/>
  <parameter name = "SpdScinXYSizeThird"
             value = "SpdModXYSize/3-2./3.*SpdBoxSideThick"/>
  <parameter name = "SpdScinXYSizeFourth"
             value = "SpdModXYSize/4-SpdBoxSideThick/2"/>

  <parameter name = "SpdHalfInnScinXSizeHalf"
             value = "SpdModXYSize/4-SpdBoxSideThick"/>

<!-- ##   Outer Module contains 4 x 4 Outer Cells wrapped in Paper    ## -->
  <parameter name = "SpdOutCellXYSize"  value = "118.26*mm"/>
  <parameter name = "SpdOutCellXYSpace"
            value = "SpdScinXYSize/4-SpdOutCellXYSize"/>

<!-- ##   Middle Module contains 8 x 8 Middle Cells wrapped in Paper    ## -->
  <parameter name = "SpdMidCellXYSize"  value = "58.93*mm"/>
  <parameter name = "SpdMidCellXYSpace"
            value = "SpdScinXYSize/8-SpdMidCellXYSize"/>

<!-- ##   Inner Module contains 12 x 12 Inner Cells wrapped in Paper    ## -->
  <parameter name = "SpdInnCellXYSize"  value = "39.16*mm"/>
  <parameter name = "SpdInnCellXYSpace"
            value = "SpdScinXYSize/12-SpdInnCellXYSize"/>

<!-- ## Inner Half Module contains 6 x 12 Inner Cells wrapped in Paper ## -->
  <parameter name = "SpdInnCellXSpace"
            value = "SpdHalfInnScinXSizeHalf/3-SpdInnCellXYSize"/>

<!--  ###                Spd Frame Parameters              ###  -->
  <parameter name = "BeamC_XSize"  value = "25.*mm"/>
  <parameter name = "BeamC_ZSize"  value = "SpdFrameLength"/>
  <parameter name = "BeamI_XSize"  value = "50.*mm"/>
  <parameter name = "BeamI_ZSize"  value = "BeamC_ZSize"/>
  <parameter name = "Beam_Thick"  value = "3.*mm"/>

  <parameter name = "Rod_Thick" value = "2.*mm"/>
  <parameter name = "Rod_Width" value = "12.*mm"/>
  <parameter name = "SpdRod_Length" value = "466.*mm"/>

  <parameter name = "Strip_Thick" value = "2.*mm"/>
  <parameter name = "Strip_Width" value = "112.*mm"/>
  <parameter name = "HalfStrip_Width" value = "Strip_Width/2."/>
  <parameter name = "SpdStrip_Length" value = "466.*mm"/>

  <parameter name = "SpdRod_Offset"
            value = "-0.5*BeamC_ZSize+Beam_Thick+0.5*Rod_Thick"/>
  <parameter name = "SpdStrip_Offset" value = "-SpdRod_Offset"/>

  <!-- ### some combinations of the above ### -->
  <parameter name = "SpdPlateXStep"   value = "SpdModXYSize-0.5*BeamC_XSize"/>
  <parameter name = "PlateZStep"   value = "BeamC_ZSize-Beam_Thick"/>
  <parameter name = "SpdHalfStripY"
            value = "-0.5*SpdModXYSize+0.5*HalfStrip_Width"/>

<!-- ********************************************************************* -->
<!-- ********************** End of GeomParam.xml ************************* -->
<!-- ********************************************************************* -->
