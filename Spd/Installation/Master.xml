<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">
<DDDB>
<!--
  **************************************************************************
  *                                                                        *
  *  Version 2.0 of the first detailed Spd description by Grigori Rybkine  *
  *                         Grigori.Rybkine@cern.ch                        *
  *                                                                        *
  **************************************************************************
-->

<!-- ****************************************************************** -->
<!--               Scintillator Pad Detector Geometry                   -->
<!-- ****************************************************************** -->
<!-- ### XY Geometry ###
     Spd Inner Square Hole: 1 * 1 Module Size - Hole for Beam Pipe;
     Spd Inner  Section: 4 * 3 Module Sizes -  1 * 1 Module Size;
     Spd Middle Section: 8 * 5 Module Sizes - 4 * 3 Module Sizes;
     Spd Outer  Section: 16 * 13 Module Sizes - 8 * 5 Module Sizes.
     ### Z Geometry ###
     Spd starts at Z=12300*mm from the interaction point with a total
     space of SpdZSize along Z; consists of a Support Frame of
     SpdFrameLength and a detector units (modules) plane of SpdModLength
     along Z.
-->
  <!--****************************************************************** -->
  <!--                 Sensitive Plane Parameters                        -->
  <!--****************************************************************** -->
  <parameter name = "SensPlaneXSize" value = "SpdXSize"/>
  <parameter name = "SensPlaneYSize" value = "SpdYSize"/>
  <parameter name = "SensPlaneThick" value = "0.1*mm"/>
  <parameter name = "SensPlaneSpacing" value = "5.0*mm"/>
  <parameter name = "SensPlaneOffset"
            value = "-0.5*SpdZSize-0.5*SensPlaneThick-SensPlaneSpacing"/>

  <parameter name = "SpdMasterZSize"
            value = "SpdZSize+2*SensPlaneSpacing+4*SensPlaneThick"/>

<!-- ****************************************************************** -->
<!--                  Logical Volumes Definition                        -->
<!-- ****************************************************************** -->

  <logvol  name = "Spd" material = "Air" magfield = "ZeroFieldMgr/ZeroField">
    <subtraction name = "Master_Total">
      <box  name  = "Master_Box"
            sizeX = "SpdXSize+SpdXMargin"
            sizeY = "SpdYSize+SpdYMargin"
            sizeZ = "SpdMasterZSize+SpdZMargin"/>

      <box  name  = "Spd_Hole"
            sizeX = "SpdModXYSize-SpdHoleMargin"
            sizeY = "SpdModXYSize-SpdHoleMargin"
            sizeZ = "SpdFake*SpdMasterZSize"/>

    </subtraction>

    <physvol name = "Sensitive_Plane"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/SensitivePlane">
      <posXYZ z = "SensPlaneOffset"/>
    </physvol>

    <physvol name = "SpdA"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/SpdLeft">
      <posXYZ x = "4.*SpdModXYSize"/>
    </physvol>

    <physvol name = "SpdC"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/SpdRight">
      <posXYZ x = "-4.*SpdModXYSize"/>
    </physvol>

  </logvol>

  <logvol  name = "SpdLeft" material = "Air">
    <subtraction name = "Master_Left">
      <box  name  = "Master_BoxLeft"
            sizeX = "0.5*SpdXSize"
            sizeY = "SpdYSize"
            sizeZ = "SpdMasterZSize"/>

      <box  name  = "Left_Hole"
            sizeX = "SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdMasterZSize"/>
            <posXYZ x = "-4.*SpdModXYSize"
                           y = "SpdDY5"/>

      <box  name  = "Subtracted_Up5"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-3.*SpdModXYSize"
                           y = "7.*SpdModXYSize+SpdDY5"/>

      <box  name  = "Subtracted_Down5"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-3.*SpdModXYSize"
                           y = "-7.*SpdModXYSize+SpdDY5"/>

      <box  name  = "Subtracted_Up6"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-1.*SpdModXYSize"
                           y = "7.*SpdModXYSize+SpdDY6"/>

      <box  name  = "Subtracted_Down6"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-1.*SpdModXYSize"
                           y = "-7.*SpdModXYSize+SpdDY6"/>

      <box  name  = "Subtracted_Up7"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "1.*SpdModXYSize"
                           y = "7.*SpdModXYSize+SpdDY7"/>

      <box  name  = "Subtracted_Down7"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "1.*SpdModXYSize"
                           y = "-7.*SpdModXYSize+SpdDY7"/>

      <box  name  = "Subtracted_Up8"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "3.*SpdModXYSize"
                           y = "7.*SpdModXYSize+SpdDY8"/>

      <box  name  = "Subtracted_Down8"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "3.*SpdModXYSize"
                           y = "-7.*SpdModXYSize+SpdDY8"/>

    </subtraction>

    <physvol name = "SpdAInner"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/InnerSectionLeft">
      <posXYZ x = "-3.*SpdModXYSize"/>
    </physvol>

    <physvol name   = "SpdAMiddle"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/MiddleSectionLeft">
      <posXYZ x = "-2.*SpdModXYSize"/>
    </physvol>

    <physvol name = "SpdAOuter"
            logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/OuterSectionLeft">
    </physvol>

  </logvol>


  <logvol  name = "SpdRight" material = "Air">
    <subtraction name = "Master_Right">
      <box  name  = "Master_BoxRight"
            sizeX = "0.5*SpdXSize"
            sizeY = "SpdYSize"
            sizeZ = "SpdMasterZSize"/>

      <box  name  = "Right_Hole"
            sizeX = "SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdMasterZSize"/>
            <posXYZ x = "4.*SpdModXYSize"
                           y = "SpdDY4"/>

      <box  name  = "Subtracted_Up4"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "3.*SpdModXYSize"
                           y = "7.*SpdModXYSize+SpdDY4"/>

      <box  name  = "Subtracted_Down4"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "3.*SpdModXYSize"
                           y = "-7.*SpdModXYSize+SpdDY4"/>


      <box  name  = "Subtracted_Up3"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "1.*SpdModXYSize"
                           y = "7.*SpdModXYSize+SpdDY3"/>

      <box  name  = "Subtracted_Down3"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "1.*SpdModXYSize"
                           y = "-7.*SpdModXYSize+SpdDY3"/>


      <box  name  = "Subtracted_Up2"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-1.*SpdModXYSize"
                           y = "7.*SpdModXYSize+SpdDY2"/>

      <box  name  = "Subtracted_Down2"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-1.*SpdModXYSize"
                           y = "-7.*SpdModXYSize+SpdDY2"/>

      <box  name  = "Subtracted_Up1"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-3.*SpdModXYSize"
                           y = "7.*SpdModXYSize+SpdDY1"/>

      <box  name  = "Subtracted_Down1"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-3.*SpdModXYSize"
                           y = "-7.*SpdModXYSize+SpdDY1"/>

    </subtraction>

    <physvol name = "SpdCInner"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/InnerSectionRight">
      <posXYZ x="3.*SpdModXYSize"/>
    </physvol>

    <physvol name   = "SpdCMiddle"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/MiddleSectionRight">
      <posXYZ x="2.*SpdModXYSize"/>
    </physvol>

    <physvol name = "SpdCOuter"
            logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/OuterSectionRight">
    </physvol>

  </logvol>

  <!--****************************************************************** -->
  <!--               Sensitive Plane Logical Volume                      -->
  <!--****************************************************************** -->
  <logvol name = "SensitivePlane"
      material = "Air"
      sensdet  = "GaussSensPlaneDet/CaloSP">
    <subtraction name = "SensitivePlane_sub">
      <box  name  = "SensPlane_Box"
            sizeX = "SensPlaneXSize"
            sizeY = "SensPlaneYSize"
            sizeZ = "SensPlaneThick"/>
      <box  name  = "SensPlane_Hole"
            sizeX = "SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "2*SensPlaneThick"/>
    </subtraction>

  </logvol>
<!-- ****************************************************************** -->
</DDDB>
