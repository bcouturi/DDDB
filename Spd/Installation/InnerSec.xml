<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">
<DDDB>
<!--
  **************************************************************************
  *                                                                        *
  *  Version 2.0 of the first detailed Spd description by Grigori Rybkine  *
  *                         Grigori.Rybkine@cern.ch                        *
  *                                                                        *
  **************************************************************************
-->

<!-- ******************************************************************* -->
<!--  Geometry of the Inner Section of the Scintillator Pad Detector     -->
<!--                    Logical Volumes Definition                       -->
<!-- ******************************************************************* -->

  <!-- %%% Spd Inner Section %%% -->

  <logvol name = "InnerSectionLeft" material="Air">

    <subtraction name = "Spd_Inner_SectionLeft">
      <box name  = "Inner_BoxLeft"
           sizeX = "SpdInnXSize*0.5"
           sizeY = "SpdInnYSize"
           sizeZ = "SpdZSize"/>

      <box  name  = "Left_Hole"
            sizeX = "SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "-1.*SpdModXYSize"
                           y = "SpdDY5"/>

      <box  name  = "LeftUp"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ y = "2.0*SpdModXYSize+SpdDY5"/>

      <box  name  = "LeftDown"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ y = "-2.0*SpdModXYSize+SpdDY5"/>

    </subtraction>

    <paramphysvol number = "2" >
      <physvol name = "Inner_Twins_Left"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/InnerTwins">
	<posXYZ y = "SpdModXYSize+SpdDY5"/>
      </physvol>
      <posXYZ y = "-2.*SpdModXYSize"/>
    </paramphysvol>

    <physvol name = "Inner_Left_One_And_Half"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/InnerLeftOneAndHalf">
             <posXYZ x = "0.25*SpdModXYSize"
                            y ="SpdDY5" />
    </physvol>

  </logvol>

  <logvol name = "InnerSectionRight" material="Air">

    <subtraction name = "Spd_Inner_SectionRight">
      <box name  = "Inner_BoxRight"
           sizeX = "SpdInnXSize*0.5"
           sizeY = "SpdInnYSize"
           sizeZ = "SpdZSize"/>

      <box  name  = "Right_Hole"
            sizeX = "SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ x = "1.*SpdModXYSize"
                           y = "SpdDY4"/>

      <box  name  = "RightUp"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ y = "2.0*SpdModXYSize+SpdDY4"/>

      <box  name  = "RightDown"
            sizeX = "2.*SpdModXYSize"
            sizeY = "SpdModXYSize"
            sizeZ = "SpdFake*SpdZSize"/>
            <posXYZ y = "-2.0*SpdModXYSize+SpdDY4"/>

    </subtraction>

    <paramphysvol number = "2" >
      <physvol name = "Inner_Twins_Right"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/InnerTwins">
	<posXYZ y = "SpdModXYSize+SpdDY4"/>
      </physvol>
      <posXYZ y = "-2.*SpdModXYSize"/>
    </paramphysvol>

    <physvol name = "Inner_Right_One_And_Half"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Installation/InnerRightOneAndHalf">
             <posXYZ x = "-0.25*SpdModXYSize"
                            y ="SpdDY4" />
    </physvol>

  </logvol>


<!-- ******************************************************************* -->
<!--               Geometry of the Inner Section Basic Unit              -->
<!-- ******************************************************************* -->

  <logvol name = "InnerTwins" material = "Air">
    <box name  = "Inner_Twins_Box"
         sizeX = "2.*SpdModXYSize"
         sizeY = "SpdModXYSize"
         sizeZ = "SpdZSize"/>

    <physvol name = "Inner_Frame_Twins"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/FrameTwins">
      <posXYZ z = "SpdFrameOffset"/>
    </physvol>

    <paramphysvol number = "2">
      <physvol name = "Inner_Modules"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Modules/InnerModule">
	<posXYZ x = "0.5*SpdModXYSize"
	        z = "SpdPlaneOffset"/>
      </physvol>
      <posXYZ x = "-SpdModXYSize"/>
    </paramphysvol>

  </logvol>

<!-- ******************************************************************* -->
<!--             Geometry of the Inner One-And-Half Units                -->
<!-- ******************************************************************* -->
<!-- *************************************************************** -->
<!--                  Inner Left One-And-Half Unit                   -->
<!--                   Logical Volume Definition                     -->
<!-- *************************************************************** -->

  <logvol name = "InnerLeftOneAndHalf" material = "Air">
    <box name  = "Inner_Left_One_And_Half_Box"
         sizeX = "1.5*SpdModXYSize"
         sizeY = "SpdModXYSize"
         sizeZ = "SpdZSize"/>

    <physvol name = "Frame_Left_One_And_Half"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/FrameLeftOneAndHalf">
      <posXYZ z = "SpdFrameOffset"/>
    </physvol>

    <physvol name = "Inner_Left_Module"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Modules/InnerModule">
      <posXYZ x = "0.25*SpdModXYSize"
	      z = "SpdPlaneOffset"/>
    </physvol>

    <physvol name = "Inner_Left_Half_Module"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Modules/InnerHalfModule">
      <posXYZ x = "-0.5*SpdModXYSize"
	      z = "SpdPlaneOffset"/>
    </physvol>

  </logvol>

<!-- *************************************************************** -->
<!--                 Inner Right One-And-Half Unit                   -->
<!--                   Logical Volume Definition                     -->
<!-- *************************************************************** -->

  <logvol name = "InnerRightOneAndHalf" material = "Air">
    <box name  = "Inner_Right_One_And_Half_Box"
         sizeX = "1.5*SpdModXYSize"
         sizeY = "SpdModXYSize"
         sizeZ = "SpdZSize"/>

    <physvol name = "Frame_Right_One_And_Half"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/FrameRightOneAndHalf">
      <posXYZ z = "SpdFrameOffset"/>
    </physvol>

    <physvol name = "Inner_Right_Module"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Modules/InnerModule">
      <posXYZ x = "-0.25*SpdModXYSize"
	      z = "SpdPlaneOffset"/>
    </physvol>

    <physvol name = "Inner_Right_Half_Module"
             logvol = "/dd/Geometry/DownstreamRegion/Spd/Modules/InnerHalfModule">
      <posXYZ x = "0.5*SpdModXYSize"
	      z = "SpdPlaneOffset"/>
    </physvol>

  </logvol>
<!-- ******************************************************************* -->
</DDDB>
