<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">
<DDDB>

<!-- ************************************************************** -->
<!-- * BEAM PIPE                                                  * -->
<!-- * Sections in Downstream Region                              * -->
<!-- *   from z = 2270 mm to z = 7620 mm                          * -->
<!-- *                                                            * -->
<!-- * UX85-3, Compensator at 13100mm, UX85-4, Pumps and valves   * -->
<!-- *                                                            * -->
<!-- * Author: Gloria Corti                                       * -->
<!-- *                                                            * -->
<!-- *   UX85-3:                                                  * -->
<!-- *     - conical sections of 10 mrad of different thickness   * -->
<!-- *       and materials                                        * -->
<!-- *     - flange                                               * -->
<!-- *   and corresponding vacuum conical sections                * -->
<!-- ************************************************************** -->

<!-- UX85-3 Pipes of Al and Be -->
 <catalog name = "UX853">
   <logvolref href="#lvUX853Downstream"/>
   <logvolref href="#lvUX853Cone12B"/>
   <logvolref href="#lvUX853Cone13"/>
   <logvolref href="#lvUX853Cone14"/>
   <logvolref href="#lvUX853Cone15"/>
   <logvolref href="#lvUX853Cone16"/>
   <logvolref href="#lvUX853Flange17A"/>
   <logvolref href="#lvUX853Flange17B"/>
   <logvolref href="#lvUX853Vacuum12B"/>
 </catalog>

 <logvol name="lvUX853Downstream">

     <physvol name   = "pvUX853Cone12B"
              logvol = "/dd/Geometry/DownstreamRegion/PipeDownstream/UX853/lvUX853Cone12B">
       <posXYZ z = "UX853Cone12BZpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone13"
              logvol = "/dd/Geometry/DownstreamRegion/PipeDownstream/UX853/lvUX853Cone13">
       <posXYZ z = "UX853Cone13Zpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone14"
              logvol = "/dd/Geometry/DownstreamRegion/PipeDownstream/UX853/lvUX853Cone14">
       <posXYZ z = "UX853Cone14Zpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone15"
              logvol = "/dd/Geometry/DownstreamRegion/PipeDownstream/UX853/lvUX853Cone15">
       <posXYZ z = "UX853Cone15Zpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone16"
              logvol = "/dd/Geometry/DownstreamRegion/PipeDownstream/UX853/lvUX853Cone16">
       <posXYZ z = "UX853Cone16Zpos"/>
     </physvol>

     <physvol name   = "pvUX853Flange17A"
              logvol = "/dd/Geometry/DownstreamRegion/PipeDownstream/UX853/lvUX853Flange17A">
       <posXYZ z = "UX853Flange17AZpos"/>
     </physvol>

     <physvol name   = "pvUX853Flange17B"
              logvol = "/dd/Geometry/DownstreamRegion/PipeDownstream/UX853/lvUX853Flange17B">
       <posXYZ z = "UX853Flange17BZpos"/>
     </physvol>

<!-- Now the vacuum inside -->
      <physvol name   = "pvUX853Vacuum12B"
               logvol = "/dd/Geometry/DownstreamRegion/PipeDownstream/UX853/lvUX853Vacuum12B">
       <posXYZ z = "0.5*UX853DownstreamLenght"/>
     </physvol>

  </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium cylindrical part for support -->
<!-- Split Rich2/Downstream                                        -->
 <logvol name = "lvUX853Cone12B" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-12B"
         sizeZ         = "UX853Cone12BLenght"
         innerRadiusPZ = "UX853Cone12BRadiusZmax"
         innerRadiusMZ = "UX853Cone12BRadiusZmin"
         outerRadiusPZ = "UX853Cone12BOuterRadius"
         outerRadiusMZ = "UX853Cone12BOuterRadius"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 2.4 mm thick -->
 <logvol name = "lvUX853Cone13" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-13"
         sizeZ         = "UX853Cone13Lenght"
         innerRadiusPZ = "UX853Cone13RadiusZmax"
         innerRadiusMZ = "UX853Cone13RadiusZmin"
         outerRadiusPZ = "UX853Cone13RadiusZmax + UX853Cone13Thick"
         outerRadiusMZ = "UX853Cone13RadiusZmin + UX853Cone13Thick"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 3.5 mm thick for welding -->
 <logvol name = "lvUX853Cone14" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-14"
         sizeZ         = "UX853Cone14Lenght"
         innerRadiusPZ = "UX853Cone14RadiusZmax"
         innerRadiusMZ = "UX853Cone14RadiusZmin"
         outerRadiusPZ = "UX853Cone14RadiusZmax + UX853Cone14Thick"
         outerRadiusMZ = "UX853Cone14RadiusZmin + UX853Cone14Thick"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Aluminium 3.5 mm thick -->
 <logvol name = "lvUX853Cone15" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-3-Cone10mrad-15"
         sizeZ         = "UX853Cone15Lenght"
         innerRadiusPZ = "UX853Cone15RadiusZmax"
         innerRadiusMZ = "UX853Cone15RadiusZmin"
         outerRadiusPZ = "UX853Cone15RadiusZmax + UX853Cone15Thick"
         outerRadiusMZ = "UX853Cone15RadiusZmin + UX853Cone15Thick"/>
 </logvol>

<!-- UX85-3 Step 10 mrad of Aluminium for welding to flange -->
 <logvol name = "lvUX853Cone16" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-3-Cone10mrad-16"
         sizeZ         = "UX853Cone16Lenght"
         innerRadiusPZ = "UX853Cone16RadiusZmax"
         innerRadiusMZ = "UX853Cone16RadiusZmin"
         outerRadiusPZ = "UX853Cone16OuterRadius"
         outerRadiusMZ = "UX853Cone16OuterRadius"/>
 </logvol>

<!-- UX85-3 Bimetallic Flange - Aluminium -->
 <logvol name = "lvUX853Flange17A" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-3-Flange-17A"
         sizeZ         = "UX853Flange17ALenght"
         innerRadiusPZ = "UX853Flange17ARadiusZmax"
         innerRadiusMZ = "UX853Flange17ARadiusZmin"
         outerRadiusPZ = "UX853Flange17AOuterRadius"
         outerRadiusMZ = "UX853Flange17AOuterRadius"/>
 </logvol>

<!-- UX85-3 Bimetallic Flange - St.Steel -->
 <logvol name = "lvUX853Flange17B" material = "Pipe/PipeSteel316LN">
   <cons name          = "UX85-3-Flange-17B"
         sizeZ         = "UX853Flange17BLenght"
         innerRadiusPZ = "UX853Flange17BRadiusZmax"
         innerRadiusMZ = "UX853Flange17BRadiusZmin"
         outerRadiusPZ = "UX853Flange17BOuterRadius"
         outerRadiusMZ = "UX853Flange17BOuterRadius"/>
 </logvol>

<!-- Vacuum inside whole of it -->
  <logvol name = "lvUX853Vacuum12B" material = "Vacuum">
   <cons name          = "UX85-3-Vacuum-12B"
         sizeZ         = "UX853DownstreamLenght"
         outerRadiusMZ = "UX853Cone12BRadiusZmin"
         outerRadiusPZ = "UX853Flange17BRadiusZmax"/>
 </logvol>

</DDDB>
